package com.sfv.uberemailservice.adapters;

public interface EmailSenderGateway {
    void sendEmail(String to,String subject, String body);
}
