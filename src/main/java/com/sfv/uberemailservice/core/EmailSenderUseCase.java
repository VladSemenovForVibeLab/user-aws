package com.sfv.uberemailservice.core;

public interface EmailSenderUseCase {
    void sendEmail(String to,String subject, String body);
}
