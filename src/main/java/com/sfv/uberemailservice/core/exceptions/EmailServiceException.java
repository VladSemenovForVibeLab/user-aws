package com.sfv.uberemailservice.core.exceptions;

public class EmailServiceException extends RuntimeException {
    public EmailServiceException(String email) {
        super(email);
    }

    public EmailServiceException(String message,Throwable cause){
        super(message,cause);
    }

}
